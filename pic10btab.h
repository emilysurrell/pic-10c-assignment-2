#ifndef PIC10BTAB_H
#define PIC10BTAB_H

#include <QWidget>
#include <QSlider>
#include <QBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QRadioButton>

class PIC10BTab : public QWidget
{
    Q_OBJECT
public:
    explicit PIC10BTab(QWidget *parent = nullptr);

public slots:
    void updateValues();

private:
    QSlider *hwsliders[8];
    QSlider *examsliders[3];
    QLabel *overall;
    QRadioButton *schemaA;
    QRadioButton *schemaB;
    QRadioButton *highest;
    double computeHomeworkTotal();
    double computeSchemaA();
    double computeSchemaB();
    double computeOverall();

signals:

};

#endif // PIC10BTAB_H
