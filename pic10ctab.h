#ifndef PIC10CTAB_H
#define PIC10CTAB_H

#include <QWidget>
#include <QSlider>
#include <QBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QRadioButton>

class PIC10CTab : public QWidget
{
    Q_OBJECT
public:
    explicit PIC10CTab(QWidget *parent = nullptr);

signals:

public slots:
    void updateValues();

private:
    QSlider *hwsliders[3];
    QSlider *midtermslider;
    QSlider *finalexamslider;
    QSlider *finalprojectslider;
    QLabel *overall;
    QRadioButton *schemaA;
    QRadioButton *schemaB;
    QRadioButton *highest;
    double computeSchemaA();
    double computeSchemaB();
    double computeOverall();

};

#endif // PIC10CTAB_H
