#include "math167tab.h"

Math167Tab::Math167Tab(QWidget *parent) : QWidget(parent)
{
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::LeftToRight);

    //homework
    QBoxLayout *hwlayout = new QBoxLayout(QBoxLayout::TopToBottom);
    QBoxLayout *hwlayouts[6];
    QLabel *hwlabels[6];
    QSpinBox *hwspinboxes[6];

    for (int i = 0; i < 6; i++)
    {

        hwlayouts[i] = new QBoxLayout(QBoxLayout::LeftToRight);
        hwlabels[i] = new QLabel(QString("HW ") + QString::number(i + 1));
        hwsliders[i] = new QSlider(Qt::Horizontal, parent);
        hwspinboxes[i] = new QSpinBox(parent);
        hwsliders[i]->setRange(0,5);
        hwsliders[i]->setTickInterval(1);
        hwspinboxes[i]->setRange(0,5);
        hwspinboxes[i]->setMaximumWidth(75);
        hwlayouts[i]->addWidget(hwlabels[i]);
        hwlayouts[i]->addWidget(hwsliders[i]);
        hwlayouts[i]->addWidget(hwspinboxes[i]);
        hwlayout->addLayout(hwlayouts[i]);
        QObject::connect(hwsliders[i], SIGNAL(valueChanged(int)), hwspinboxes[i], SLOT(setValue(int)));
        QObject::connect(hwspinboxes[i], SIGNAL(valueChanged(int)), hwsliders[i], SLOT(setValue(int)));

    }

    //quizzes
    QBoxLayout *quizlayout = new QBoxLayout(QBoxLayout::TopToBottom);
    QBoxLayout *quizlayouts[6];
    QLabel *quizlabels[6];
    QSpinBox *quizspinboxes[6];
    for (int i = 0; i < 6; i++)
    {
        quizlayouts[i] = new QBoxLayout(QBoxLayout::LeftToRight);
        quizlabels[i] = new QLabel(QString("Quiz ") + QString::number(i + 1));
        quizsliders[i] = new QSlider(Qt::Horizontal, parent);
        quizspinboxes[i] = new QSpinBox(parent);
        quizsliders[i]->setRange(0,4);
        quizsliders[i]->setTickInterval(1);
        quizspinboxes[i]->setRange(0,4);
        quizspinboxes[i]->setMaximumWidth(75);
        quizlayouts[i]->addWidget(quizlabels[i]);
        quizlayouts[i]->addWidget(quizsliders[i]);
        quizlayouts[i]->addWidget(quizspinboxes[i]);
        quizlayout->addLayout(quizlayouts[i]);
        QObject::connect(quizsliders[i], SIGNAL(valueChanged(int)), quizspinboxes[i], SLOT(setValue(int)));
        QObject::connect(quizspinboxes[i], SIGNAL(valueChanged(int)), quizsliders[i], SLOT(setValue(int)));
    }

    //exams
    QBoxLayout *examlayout = new QBoxLayout(QBoxLayout::TopToBottom);

    //midterm1
    QBoxLayout *midterm1layout = new QBoxLayout(QBoxLayout::LeftToRight);
    QLabel *midterm1label = new QLabel("Midterm 1");
    midterm1slider = new QSlider(Qt::Horizontal, parent);
    QSpinBox *midterm1spinbox = new QSpinBox(parent);
    midterm1slider->setRange(0,15);
    midterm1slider->setTickInterval(1);
    midterm1spinbox->setRange(0,15);
    midterm1spinbox->setMaximumWidth(75);
    midterm1layout->addWidget(midterm1label);
    midterm1layout->addWidget(midterm1slider);
    midterm1layout->addWidget(midterm1spinbox);
    examlayout->addSpacing(100);
    examlayout->addLayout(midterm1layout);
    QObject::connect(midterm1slider, SIGNAL(valueChanged(int)), midterm1spinbox, SLOT(setValue(int)));
    QObject::connect(midterm1spinbox, SIGNAL(valueChanged(int)), midterm1slider, SLOT(setValue(int)));

    //midterm2
    QBoxLayout *midterm2layout = new QBoxLayout(QBoxLayout::LeftToRight);
    QLabel *midterm2label = new QLabel("Midterm 2");
    midterm2slider = new QSlider(Qt::Horizontal, parent);
    QSpinBox *midterm2spinbox = new QSpinBox(parent);
    midterm2slider->setRange(0,17);
    midterm2slider->setTickInterval(1);
    midterm2spinbox->setRange(0,17);
    midterm2spinbox->setMaximumWidth(75);
    midterm2layout->addWidget(midterm2label);
    midterm2layout->addWidget(midterm2slider);
    midterm2layout->addWidget(midterm2spinbox);
    examlayout->addSpacing(100);
    examlayout->addLayout(midterm2layout);
    QObject::connect(midterm2slider, SIGNAL(valueChanged(int)), midterm2spinbox, SLOT(setValue(int)));
    QObject::connect(midterm2spinbox, SIGNAL(valueChanged(int)), midterm2slider, SLOT(setValue(int)));

    //final
    QBoxLayout *finallayout = new QBoxLayout(QBoxLayout::LeftToRight);
    QLabel *finallabel = new QLabel("Final Exam");
    finalslider = new QSlider(Qt::Horizontal, parent);
    QSpinBox *finalspinbox = new QSpinBox(parent);
    finalslider->setRange(0,30);
    finalslider->setTickInterval(1);
    finalspinbox->setRange(0,30);
    finalspinbox->setMaximumWidth(75);
    finallayout->addWidget(finallabel);
    finallayout->addWidget(finalslider);
    finallayout->addWidget(finalspinbox);
    examlayout->addSpacing(100);
    examlayout->addLayout(finallayout);
    QObject::connect(finalslider, SIGNAL(valueChanged(int)), finalspinbox, SLOT(setValue(int)));
    QObject::connect(finalspinbox, SIGNAL(valueChanged(int)), finalslider, SLOT(setValue(int)));

    overall = new QLabel("\tOverall Score:    " + QString::number(computeOverall()));
    examlayout->addWidget(overall);

    //connect everything
    for (int i = 0; i < 6; i++)
        QObject::connect(hwsliders[i], SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    for (int i = 0; i < 6; i++)
        QObject::connect(quizsliders[i], SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    QObject::connect(midterm1slider, SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    QObject::connect(midterm2slider, SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    QObject::connect(finalslider, SIGNAL(valueChanged(int)), this, SLOT(updateValues()));

    //put it all together
    layout->addLayout(hwlayout);
    layout->addSpacing(20);
    layout->addLayout(quizlayout);
    layout->addSpacing(20);
    layout->addLayout(examlayout);
    setLayout(layout);
}

int Math167Tab::computeHomeworkTotal()
{
    // each homework has 5 possible points out of 4
    // lowest homework is dropped
    // one point = one percent of grade (each hw is worth 4% of grade)
    int hwsum = 0;
    int hwmin = 5;
    for(int i = 0; i < 6; i++)
    {
        hwsum += hwsliders[i]->value();
        if (hwsliders[i]->value() < hwmin)
            hwmin = hwsliders[i]->value();
    }
    return hwsum - hwmin;
}

int Math167Tab::computeQuizTotal()
{
    // each quiz has 4 possible points out of 4
    // lowest quiz is dropped
    // one point = one percent of grade (each quiz is worth 4% of grade)
    int quizsum = 0;
    int quizmin = 4;
    for(int i = 0; i < 6; i++)
    {
        quizsum += quizsliders[i]->value();
        if (quizsliders[i]->value() < quizmin)
            quizmin = quizsliders[i]->value();
    }
    return quizsum - quizmin;
}

int Math167Tab::computeOverall()
{
    // midterm 1 has 15 possible points out of 15, worth 15% of grade
    // midterm 2 has 17 possible points out of 15, worth 15% of grade
    // final exam has 30 possible points out of 30, worth 30% of grade
    // one point = one percent of grade
    return computeHomeworkTotal() + computeQuizTotal() + midterm1slider->value() + midterm2slider->value() + finalslider->value();
}

void Math167Tab::updateValues()
{
    overall->setText("\tOverall Score:    " + QString::number(computeOverall()));
}
