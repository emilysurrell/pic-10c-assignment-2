#include "pic10ctab.h"

PIC10CTab::PIC10CTab(QWidget *parent) : QWidget(parent)
{
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);

    //top section
    QGridLayout *toplayout = new QGridLayout();
    toplayout->setVerticalSpacing(10);
    toplayout->setHorizontalSpacing(40);
    QLabel *hwlabels[3];
    QSpinBox *hwspinboxes[3];
    for (int i = 0; i < 3; i++)
    {
        hwlabels[i] = new QLabel(QString("\n\nHomework ") + QString::number(i + 1));
        hwsliders[i] = new QSlider(Qt::Horizontal, parent);
        hwspinboxes[i] = new QSpinBox(parent);
        hwsliders[i]->setRange(0,20);
        hwsliders[i]->setTickInterval(1);
        hwspinboxes[i]->setRange(0,20);
        toplayout->addWidget(hwlabels[i], 3*i, 0);
        toplayout->addWidget(hwsliders[i], 3*i + 1, 0);
        toplayout->addWidget(hwspinboxes[i], 3*i + 2, 0);
        QObject::connect(hwsliders[i], SIGNAL(valueChanged(int)), hwspinboxes[i], SLOT(setValue(int)));
        QObject::connect(hwspinboxes[i], SIGNAL(valueChanged(int)), hwsliders[i], SLOT(setValue(int)));
    }

    QLabel *midtermlabel = new QLabel("\n\nMidterm");
    QLabel *finalexamlabel = new QLabel("\n\nFinal Exam");
    QLabel *finalprojectlabel = new QLabel("\n\nFinal Project");

    midtermslider = new QSlider(Qt::Horizontal, parent);
    midtermslider->setRange(0,37);
    midtermslider->setTickInterval(1);
    finalexamslider = new QSlider(Qt::Horizontal, parent);
    finalexamslider->setRange(0,100);
    finalexamslider->setTickInterval(1);
    finalprojectslider = new QSlider(Qt::Horizontal, parent);
    finalprojectslider->setRange(0,100);
    finalprojectslider->setTickInterval(1);

    QSpinBox *midtermspinbox = new QSpinBox(parent);
    midtermspinbox->setRange(0,37);
    QSpinBox *finalexamspinbox = new QSpinBox(parent);
    finalexamspinbox->setRange(0,100);
    QSpinBox *finalprojectspinbox = new QSpinBox(parent);
    finalprojectspinbox->setRange(0,100);

    QObject::connect(midtermslider, SIGNAL(valueChanged(int)), midtermspinbox, SLOT(setValue(int)));
    QObject::connect(midtermspinbox, SIGNAL(valueChanged(int)), midtermslider, SLOT(setValue(int)));
    QObject::connect(finalexamslider, SIGNAL(valueChanged(int)), finalexamspinbox, SLOT(setValue(int)));
    QObject::connect(finalexamspinbox, SIGNAL(valueChanged(int)), finalexamslider, SLOT(setValue(int)));
    QObject::connect(finalprojectslider, SIGNAL(valueChanged(int)), finalprojectspinbox, SLOT(setValue(int)));
    QObject::connect(finalprojectspinbox, SIGNAL(valueChanged(int)), finalprojectslider, SLOT(setValue(int)));

    toplayout->addWidget(midtermlabel, 0, 1);
    toplayout->addWidget(midtermslider, 1, 1);
    toplayout->addWidget(midtermspinbox, 2, 1);
    toplayout->addWidget(finalexamlabel, 3, 1);
    toplayout->addWidget(finalexamslider, 4, 1);
    toplayout->addWidget(finalexamspinbox, 5, 1);
    toplayout->addWidget(finalprojectlabel, 6, 1);
    toplayout->addWidget(finalprojectslider, 7, 1);
    toplayout->addWidget(finalprojectspinbox, 8, 1);

    //bottom section
    QBoxLayout *bottomlayout = new QBoxLayout(QBoxLayout::LeftToRight);
    QBoxLayout *radiobuttons = new QBoxLayout(QBoxLayout::TopToBottom);
    schemaA = new QRadioButton("Schema A", parent);
    schemaB = new QRadioButton("Schema B", parent);
    highest = new QRadioButton("Highest", parent);
    radiobuttons->addSpacing(10);
    radiobuttons->addWidget(schemaA);
    radiobuttons->addWidget(schemaB);
    radiobuttons->addWidget(highest);
    bottomlayout->addSpacing(200);
    bottomlayout->addLayout(radiobuttons);

    overall = new QLabel("Overall Score:    " + QString::number(computeOverall()) + "  (N/A)");
    bottomlayout->addWidget(overall);

    //connect everything
    for (int i = 0; i < 3; i++)
        QObject::connect(hwsliders[i], SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    QObject::connect(midtermslider, SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    QObject::connect(finalexamslider, SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    QObject::connect(finalprojectslider, SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    QObject::connect(schemaA, SIGNAL(toggled(bool)), this, SLOT(updateValues()));
    QObject::connect(schemaB, SIGNAL(toggled(bool)), this, SLOT(updateValues()));
    QObject::connect(highest, SIGNAL(toggled(bool)), this, SLOT(updateValues()));

    //put it all together
    layout->addLayout(toplayout);
    layout->addLayout(bottomlayout);
    setLayout(layout);
}

double PIC10CTab::computeSchemaA()
{
    double hw_total = ((hwsliders[0]->value() + hwsliders[1]->value() + hwsliders[2]->value())/ 60.0) * 15.0;
    double mt = (midtermslider->value() / 37.0) * 25.0;
    double fexam = (finalexamslider->value() / 100.0) * 30.0;
    double fproj = (finalprojectslider->value() / 100.0) * 35.0;
    return hw_total + mt + fexam + fproj;
}

double PIC10CTab::computeSchemaB()
{
    double hw_total = ((hwsliders[0]->value() + hwsliders[1]->value() + hwsliders[2]->value())/ 60.0) * 15.0;
    double fexam = (finalexamslider->value() / 100.0) * 50.0;
    double fproj = (finalprojectslider->value() / 100.0) * 35.0;
    return hw_total + fexam + fproj;
}

double PIC10CTab::computeOverall()
{
    double schemaAscore = computeSchemaA();
    double schemaBscore = computeSchemaB();
    if (schemaBscore > schemaAscore)
        return schemaBscore;
    return schemaAscore;
}

void PIC10CTab::updateValues()
{
    double score;
    if (schemaA->isChecked())
        score = computeSchemaA();
    else if (schemaB->isChecked())
        score = computeSchemaB();
    else
        score = computeOverall();

    QString lettergrade = "  (N/A)";
    if (score >= 93.33)
        lettergrade = "  (A)";
    else if (score >= 90.0)
        lettergrade = "  (A-)";
    else if (score >= 86.66)
        lettergrade = "  (B+)";
    else if (score >= 83.33)
        lettergrade = "  (B)";
    else if (score >= 80.0)
        lettergrade = "  (B-)";
    else if (score >= 76.66)
        lettergrade = "  (C+)";
    else if (score >= 73.33)
        lettergrade = "  (C)";
    else if (score >= 70.0)
        lettergrade = "  (C-)";

    overall->setText("Overall Score:    " + QString::number(score) + lettergrade);
}
