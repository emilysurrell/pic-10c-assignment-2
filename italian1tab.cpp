#include "italian1tab.h"

Italian1Tab::Italian1Tab(QWidget *parent) : QWidget(parent)
{
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::LeftToRight);

    QBoxLayout *leftlayout = new QBoxLayout(QBoxLayout::TopToBottom);

    QBoxLayout *compositionlayouts[3];
    QLabel *compositionlabels[3];
    QSpinBox *compositionspinboxes[3];
    for (int i = 0; i < 3; i++)
    {
        compositionlayouts[i] = new QBoxLayout(QBoxLayout::LeftToRight);
        compositionlabels[i] = new QLabel(QString("Composition ") + QString::number(i + 1));
        compositionsliders[i] = new QSlider(Qt::Horizontal, parent);
        compositionspinboxes[i] = new QSpinBox(parent);
        compositionsliders[i]->setRange(0,100);
        compositionsliders[i]->setTickInterval(1);
        compositionspinboxes[i]->setRange(0,100);
        compositionspinboxes[i]->setMaximumWidth(75);
        compositionlayouts[i]->addWidget(compositionlabels[i]);
        compositionlayouts[i]->addWidget(compositionsliders[i]);
        compositionlayouts[i]->addWidget(compositionspinboxes[i]);
        leftlayout->addLayout(compositionlayouts[i]);
        QObject::connect(compositionsliders[i], SIGNAL(valueChanged(int)), compositionspinboxes[i], SLOT(setValue(int)));
        QObject::connect(compositionspinboxes[i], SIGNAL(valueChanged(int)), compositionsliders[i], SLOT(setValue(int)));
        QObject::connect(compositionsliders[i], SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    }

    QBoxLayout *quizlayouts[3];
    QLabel *quizlabels[3];
    QSpinBox *quizspinboxes[3];
    for (int i = 0; i < 3; i++)
    {
        quizlayouts[i] = new QBoxLayout(QBoxLayout::LeftToRight);
        quizlabels[i] = new QLabel(QString("           Quiz ") + QString::number(i + 1));
        quizsliders[i] = new QSlider(Qt::Horizontal, parent);
        quizspinboxes[i] = new QSpinBox(parent);
        quizsliders[i]->setRange(0,100);
        quizsliders[i]->setTickInterval(1);
        quizspinboxes[i]->setRange(0,100);
        quizspinboxes[i]->setMaximumWidth(75);
        quizlayouts[i]->addWidget(quizlabels[i]);
        quizlayouts[i]->addWidget(quizsliders[i]);
        quizlayouts[i]->addWidget(quizspinboxes[i]);
        leftlayout->addLayout(quizlayouts[i]);
        QObject::connect(quizsliders[i], SIGNAL(valueChanged(int)), quizspinboxes[i], SLOT(setValue(int)));
        QObject::connect(quizspinboxes[i], SIGNAL(valueChanged(int)), quizsliders[i], SLOT(setValue(int)));
        QObject::connect(quizsliders[i], SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    }

    QBoxLayout *rightlayout = new QBoxLayout(QBoxLayout::LeftToRight);

    QBoxLayout *participationlayout = new QBoxLayout(QBoxLayout::TopToBottom);
    QBoxLayout *hwlayout = new QBoxLayout(QBoxLayout::TopToBottom);
    QBoxLayout *finallayout = new QBoxLayout(QBoxLayout::TopToBottom);

    QLabel *participationlabel = new QLabel("Participation");
    QLabel *hwlabel = new QLabel("Homework");
    QLabel *finallabel = new QLabel("Final Exam");

    QSpinBox *participationspinbox = new QSpinBox(parent);
    participationspinbox->setRange(0,19);
    participationspinbox->setMaximumWidth(75);
    QSpinBox *hwspinbox = new QSpinBox(parent);
    hwspinbox->setRange(0,100);
    hwspinbox->setMaximumWidth(75);
    QSpinBox *finalspinbox = new QSpinBox(parent);
    finalspinbox->setRange(0,100);
    finalspinbox->setMaximumWidth(75);

    participationslider = new QSlider(Qt::Vertical, parent);
    participationslider->setRange(0,19);
    participationslider->setTickInterval(1);
    QObject::connect(participationslider, SIGNAL(valueChanged(int)), participationspinbox, SLOT(setValue(int)));
    QObject::connect(participationspinbox, SIGNAL(valueChanged(int)), participationslider, SLOT(setValue(int)));
    QObject::connect(participationslider, SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    hwslider = new QSlider(Qt::Vertical, parent);
    hwslider->setRange(0,100);
    hwslider->setTickInterval(1);
    QObject::connect(hwslider, SIGNAL(valueChanged(int)), hwspinbox, SLOT(setValue(int)));
    QObject::connect(hwspinbox, SIGNAL(valueChanged(int)), hwslider, SLOT(setValue(int)));
    QObject::connect(hwslider, SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    finalslider = new QSlider(Qt::Vertical, parent);
    finalslider->setRange(0,100);
    finalslider->setTickInterval(1);
    QObject::connect(finalslider, SIGNAL(valueChanged(int)), finalspinbox, SLOT(setValue(int)));
    QObject::connect(finalspinbox, SIGNAL(valueChanged(int)), finalslider, SLOT(setValue(int)));
    QObject::connect(finalslider, SIGNAL(valueChanged(int)), this, SLOT(updateValues()));

    participationlayout->addWidget(participationlabel);
    hwlayout->addWidget(hwlabel);
    finallayout->addWidget(finallabel);

    participationlayout->addWidget(participationspinbox);
    hwlayout->addWidget(hwspinbox);
    finallayout->addWidget(finalspinbox);

    participationlayout->addWidget(participationslider);
    hwlayout->addWidget(hwslider);
    finallayout->addWidget(finalslider);

    rightlayout->addLayout(participationlayout);
    rightlayout->addLayout(hwlayout);
    rightlayout->addLayout(finallayout);

    overall = new QLabel("Overall Score:    " + QString::number(computeOverall()));
    overall->setFixedWidth(150);
    rightlayout->addWidget(overall);

    layout->addLayout(leftlayout);
    layout->addSpacing(30);
    layout->addLayout(rightlayout);
    setLayout(layout);
}

double Italian1Tab::computeCompositionTotal()
{
    //each composition is out of 100 points
    //compositions in total are worth 20% of grade
    return ((compositionsliders[0]->value() + compositionsliders[1]->value() + compositionsliders[2]->value()) / 300.0) * 20.0;
}

double Italian1Tab::computeQuizTotal()
{
    //each quiz is out of 100 points
    //quizzes in total are worth 30% of grade
    return ((quizsliders[0]->value() + quizsliders[1]->value() + quizsliders[2]->value()) / 300.0) * 30.0;
}

double Italian1Tab::computeOverall()
{
    //participation is out of 19 points, worth 15% of grade
    double part = (participationslider->value() / 19.0) * 15.0;
    //hw is out of 100 points, worth 10% of grade
    double hw = (hwslider->value() / 100.0) * 10.0;
    //final is out of 100 points, worth 25% of grade
    double final = (finalslider->value() / 100.0) * 25.0;
    return part + hw + computeCompositionTotal() + computeQuizTotal() + final;
}

void Italian1Tab::updateValues()
{
    overall->setText("Overall Score:    " + QString::number(computeOverall()));
}
