# README #

## This repository is for PIC 10C, Assignment 2 at UCLA ##

Quarter: Spring 2020

Instructor: Ricardo Salazar

Student: Emily Surrell

### The Assignment ###

* This project creates a grade calculator widget in Qt
* This calculator computes grades for four different courses: PIC 10B, PIC 10C, Math 167, and Italian 1
* Version Control: See my [Commits](https://bitbucket.org/emilysurrell/pic-10c-assignment-2/commits/) for this repository
* Read the [Assignment Description](https://www.pic.ucla.edu/~rsalazar/spring2020/assignments/hw2/)