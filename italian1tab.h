#ifndef ITALIAN1TAB_H
#define ITALIAN1TAB_H

#include <QWidget>
#include <QSlider>
#include <QBoxLayout>
#include <QLabel>
#include <QSpinBox>

class Italian1Tab : public QWidget
{
    Q_OBJECT
public:
    explicit Italian1Tab(QWidget *parent = nullptr);

public slots:
    void updateValues();

private:
    QSlider *participationslider;
    QSlider *hwslider;
    QSlider *compositionsliders[3];
    QSlider *quizsliders[3];
    QSlider *finalslider;
    QLabel *overall;
    double computeCompositionTotal();
    double computeQuizTotal();
    double computeOverall();
signals:

};

#endif // ITALIAN1TAB_H
