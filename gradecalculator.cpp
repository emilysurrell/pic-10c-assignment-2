#include "gradecalculator.h"
#include "pic10btab.h"
#include "pic10ctab.h"
#include "math167tab.h"
#include "italian1tab.h"
#include "ui_gradecalculator.h"

GradeCalculator::GradeCalculator(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::GradeCalculator)
{
    ui->setupUi(this);

    tabs = new QTabWidget;
    tabs->addTab(new PIC10BTab(),"PIC 10B");
    tabs->addTab(new PIC10CTab(),"PIC 10C");
    tabs->addTab(new Math167Tab(),"Math 167");
    tabs->addTab(new Italian1Tab(),"Italian 1");
    setCentralWidget(tabs);
}

GradeCalculator::~GradeCalculator()
{
    delete ui;
}

