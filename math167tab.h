#ifndef MATH167TAB_H
#define MATH167TAB_H

#include <QWidget>
#include <QSlider>
#include <QBoxLayout>
#include <QLabel>
#include <QSpinBox>

class Math167Tab : public QWidget
{
    Q_OBJECT
public:
    explicit Math167Tab(QWidget *parent = nullptr);

public slots:
    void updateValues();

private:
    QSlider *hwsliders[6];
    QSlider *quizsliders[6];
    QSlider *midterm1slider;
    QSlider *midterm2slider;
    QSlider *finalslider;
    QLabel *overall;
    int computeHomeworkTotal();
    int computeQuizTotal();
    int computeOverall();
signals:

};

#endif // MATH167TAB_H
