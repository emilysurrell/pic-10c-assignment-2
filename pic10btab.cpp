#include "pic10btab.h"

PIC10BTab::PIC10BTab(QWidget *parent) : QWidget(parent)
{
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::LeftToRight);

    //homework box
    QBoxLayout *hwlayout = new QBoxLayout(QBoxLayout::TopToBottom);
    QBoxLayout *hwlayouts[8];
    QLabel *hwlabels[8];
    QSpinBox *hwspinboxes[8];
    for (int i = 0; i < 8; i++)
    {
        hwlayouts[i] = new QBoxLayout(QBoxLayout::LeftToRight);
        hwlabels[i] = new QLabel(QString("Hw ") + QString::number(i + 1));
        hwsliders[i] = new QSlider(Qt::Horizontal, parent);
        hwspinboxes[i] = new QSpinBox(parent);
        hwsliders[i]->setRange(0,100);
        hwsliders[i]->setTickInterval(1);
        hwspinboxes[i]->setRange(0,100);
        hwspinboxes[i]->setMaximumWidth(75);
        hwlayouts[i]->addWidget(hwlabels[i]);
        hwlayouts[i]->addWidget(hwsliders[i]);
        hwlayouts[i]->addWidget(hwspinboxes[i]);
        hwlayout->addLayout(hwlayouts[i]);
        QObject::connect(hwsliders[i], SIGNAL(valueChanged(int)), hwspinboxes[i], SLOT(setValue(int)));
        QObject::connect(hwspinboxes[i], SIGNAL(valueChanged(int)), hwsliders[i], SLOT(setValue(int)));
    }
    layout->addLayout(hwlayout);

    //exams box
    QBoxLayout *examlayout = new QBoxLayout(QBoxLayout::TopToBottom);
    QLabel *examlabels[3] = {new QLabel("\n\nMidterm 1"), new QLabel("\n\nMidterm 2"), new QLabel("\n\nFinal Exam")};
    QSpinBox *examspinboxes[3];
    for (int i = 0; i < 3; i++)
    {
       examsliders[i] = new QSlider(Qt::Horizontal, parent);
       examspinboxes[i]= new QSpinBox(parent);
       examsliders[i]->setRange(0,100);
       examsliders[i]->setTickInterval(1);
       examspinboxes[i]->setRange(0,100);
       examlayout->addWidget(examlabels[i]);
       examlayout->addWidget(examsliders[i]);
       examlayout->addWidget(examspinboxes[i]);
       QObject::connect(examsliders[i], SIGNAL(valueChanged(int)), examspinboxes[i], SLOT(setValue(int)));
       QObject::connect(examspinboxes[i], SIGNAL(valueChanged(int)), examsliders[i], SLOT(setValue(int)));
    }

    //bottom section
    schemaA = new QRadioButton("Schema A", parent);
    schemaB = new QRadioButton("Schema B", parent);
    highest = new QRadioButton("Highest", parent);

    overall = new QLabel("Overall Score:    " + QString::number(computeOverall()));

    //connect everything
    for (int i = 0; i < 8; i++)
        QObject::connect(hwsliders[i], SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    for (int i = 0; i < 3; i++)
        QObject::connect(examsliders[i], SIGNAL(valueChanged(int)), this, SLOT(updateValues()));
    QObject::connect(schemaA, SIGNAL(toggled(bool)), this, SLOT(updateValues()));
    QObject::connect(schemaB, SIGNAL(toggled(bool)), this, SLOT(updateValues()));
    QObject::connect(highest, SIGNAL(toggled(bool)), this, SLOT(updateValues()));

    examlayout->addSpacing(30); //blank space
    examlayout->addWidget(schemaA);
    examlayout->addWidget(schemaB);
    examlayout->addWidget(highest);
    examlayout->addWidget(overall);
    layout->addSpacing(30); //blank space between columns
    layout->addLayout(examlayout);

    setLayout(layout);
}

double PIC10BTab::computeHomeworkTotal()
{
    double min = 100;  //lowest homework score will be dropped
    double sum = 0;
    for (int i = 0; i < 8; i++)
    {
        sum += hwsliders[i]->value();
        if (hwsliders[i]->value() < min)
            min = hwsliders[i]->value();
    }
    sum -= min; //dropping lowest score
    sum/=7.0; //computing average
    sum/=100.0; //each assignment is out of 100 points
    sum*=25; //hw category is worth 25 percent of grade
    return sum;
}

double PIC10BTab::computeSchemaA()
{
    double hw_total = computeHomeworkTotal();
    double mt1 = (examsliders[0]->value() / 100.0) * 20.0;
    double mt2 = (examsliders[1]->value() / 100.0) * 20.0;
    double final = (examsliders[2]->value() / 100.0) * 35.0;
    return hw_total + mt1 + mt2 + final;
}

double PIC10BTab::computeSchemaB()
{
    double hw_total = computeHomeworkTotal();
    double highermidterm = examsliders[0]->value();
    if (examsliders[1]->value() > highermidterm)
        highermidterm = examsliders[1]->value();
    double mt = (highermidterm / 100.0) * 30.0;
    double final = (examsliders[2]->value() / 100.0) * 44.0;
    return hw_total + mt + final;
}

double PIC10BTab::computeOverall()
{
    double schemaAscore = computeSchemaA();
    double schemaBscore = computeSchemaB();
    if (schemaBscore > schemaAscore)
        return schemaBscore;
    return schemaAscore;
}

void PIC10BTab::updateValues()
{
    if (schemaA->isChecked())
        overall->setText("Overall Score:    " + QString::number(computeSchemaA()));
    else if (schemaB->isChecked())
        overall->setText("Overall Score:    " + QString::number(computeSchemaB()));
    else
        overall->setText("Overall Score:    " + QString::number(computeOverall()));
}
